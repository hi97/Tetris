document.getElementById('hello_text').textContent = 'はじめてのJavaScript';

let count = 0,
    cells,
    isFalling = false,
    fallingBlockNum = 0,
    isRightEnd, isLeftEnd = false;

const blocks = {
    i: {
        class: 'i',
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: 'o',
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: 't',
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: 's',
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: 'z',
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: 'j',
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: 'l',
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    },
}

let currentBlock;

const onkeydown = (event) => {
    switch (event.keyCode) {
        case 32:
            return rotate();
        case 37:
            return moveLeft();
        case 39:
            return moveRight();
        case 40:
            return fallBlocks();
        default:
            return;
    }
}
document.addEventListener('keydown', onkeydown);

const loadTabel = () => {
    cells = [];
    const td_array = document.getElementsByTagName('td');
    let index = 0;

    for (let row = 0; row < 20; row++) {
        cells[row] = [];
        for (let col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

const fallBlocks = () => {
    for (let col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {
            isFalling = false;
            return;
        }
    }

    for (let row = 18; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== '' && cells[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return;
                }
            }
        }
    }

    for (let row = 18; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = '';
                cells[row][col].blockNum = null;
            }
        }
    }
}

const hasFallingBlock = () => {
    return isFalling;
}

const deleteRow = () => {
    for (let row = 19; row >= 0; row--) {
        let canDelete = true;
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].className === '') {
                canDelete = false;
            }
        }
        if (canDelete) {
            for (let col = 0; col < 10; col++) {
                cells[row][col].className = '';
            }
            for (let downRow = row - 1; row >= 0; row--) {
                downRow = row - 1;
                for (let col = 0; col < 10; col++) {
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = '';
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}

const generateBlock = () => {
    const keys = Object.keys(blocks);
    const nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    const nextBlock = blocks[nextBlockKey];
    const nextFallingBlockNum = fallingBlockNum + 1;

    const pattern = nextBlock.pattern;
    for (let row = 0; row < pattern.length; row++) {
        for (let col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    currentBlock = Object.assign({}, nextBlock);
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

const moveRight = () => {
    for (let row = 19; row >= 0; row--) {
        if (cells[row][9].blockNum === fallingBlockNum) {
            return isRightEnd = true;
        }
    }

    for (let row = 19; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col + 1].className !== '' && cells[row][col + 1].blockNum !== fallingBlockNum) {
                    return isRightEnd = true;
                }
            }
        }
    }

    for (let row = 0; row < 20; row++) {
        for (let col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = '';
                cells[row][col].blockNum = null;
            }
        }
    }
    return isRightEnd = false;
}

const moveLeft = () => {
    for (let row = 19; row >= 0; row--) {
        if (cells[row][0].blockNum === fallingBlockNum) {
            return isLeftEnd = true;
        }
    }

    for (let row = 19; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col - 1].className !== '' && cells[row][col - 1].blockNum !== fallingBlockNum) {
                    return isLeftEnd = true;
                }
            }
        }
    }

    for (let row = 0; row < 20; row++) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = '';
                cells[row][col].blockNum = null;
            }
        }
    }
    return isLeftEnd = false;
}

const rotate = () => { // 未完成
    for (let row = 0; row < 20; row++) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (currentBlock.class === 'o') {
                    return;
                }
                currentBlock = Object.assign({}, currentBlock);
            }
        }
    }

    let pattern = currentBlock.pattern;
    let rowMax, colMax;
    rowMax = pattern.length;
    for (let row = 0; row < pattern.length; row++) {
        colMax = pattern[row].length;
    }

    let temp = [];
    for (let col = 0; col < colMax; col++) {
        temp[col] = []
        for (let row = 0; row < rowMax; row++) {
            temp[col][rowMax - 1 - row] = pattern[row][col];
        }
    }
    currentBlock.pattern = temp.concat();

    let rowCenter, colCenter;
    let count = 0, max = 0;
    for (let row = 0; row < 20; row++) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                count = 0;
                for (let i = -1; i <= 1; i++) {
                    for (let j = -1; j <= 1; j++) {
                        if (cells[row + i][col + j].blockNum === fallingBlockNum) {
                            count++;
                        }
                    }
                }
                if (count >= max) {
                    max = count;
                    rowCenter = row;
                    colCenter = col;
                }
                cells[row][col].className = '';
                cells[row][col].blockNum = null;
            }
        }
    }

    pattern = currentBlock.pattern;
    for (let row = 0; row < pattern.length; row++) {
        for (let col = 0; col < pattern[row].length; col++) {
            if (currentBlock.pattern[row][col]) {
                if (pattern.length === 3) {
                    cells[rowCenter + row][colCenter + col].className = currentBlock.class;
                    cells[rowCenter + row][colCenter + col].blockNum = fallingBlockNum;
                } else if (pattern.length === 2) {
                    cells[rowCenter + row - 1][colCenter + col - 1].className = currentBlock.class;
                    cells[rowCenter + row - 1][colCenter + col - 1].blockNum = fallingBlockNum;
                } else {
                    cells[rowCenter + row - 1][colCenter + col - 1].className = currentBlock.class;
                    cells[rowCenter + row - 1][colCenter + col - 1].blockNum = fallingBlockNum;
                }
            }
        }
    }
}


loadTabel();
setInterval(() => {
    count++;
    document.getElementById('hello_text').textContent = `はじめてのJavaScript(${count})`;
    for (let row = 0; row < 2; row++) {
        for (let col = 0; col < 10; col++) {
            if (cells[row][col].className !== '' && cells[row][col].blockNum !== fallingBlockNum) {
                alert('game over');
                const restart = confirm('restart?');
                return restart ? location.reload() : location.reload();
            }
        }
    }
    if (hasFallingBlock()) {
        fallBlocks();
    } else {
        deleteRow();
        generateBlock();
    }
}, 1000);
